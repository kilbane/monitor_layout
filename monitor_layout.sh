#!/bin/sh
if [[ "$(cat /sys/class/drm/card0/subsystem/card1-HDMI-A-1/status)" == "connected" ]]; then
  layout=$(echo -e "Internal\nExternal\nMulti" | dmenu)
else
  layout="Internal"
fi
if [[ $layout == "Internal" ]]; then
  xrandr --output eDP-1 --primary --mode 1920x1080 --output HDMI-1 --off
elif [[ $layout == "External" ]]; then
  xrandr --output HDMI-1 --primary --mode 1920x1080 --output eDP-1 --off
elif [[ $layout == "Multi" ]]; then
  xrandr --output eDP-1 --primary --mode 1920x1080 --output HDMI-1 --mode 1920x1080 --right-of eDP-1
fi

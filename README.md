# Monitor Layout
This is a script for choosing which monitors to use.

## Dependencies
- xrandr
- dmenu
- cat
- echo

## Usage
If you have an external monitor connected, simply choose the option in dmenu and it will be applied. The three options are:
- Internal: Use your device's internal monitor only.
- External: Use your external monitor only.
- Multi: Use both monitors.

If you don't have an external monitor connected, running the script will set the layout to Internal.
